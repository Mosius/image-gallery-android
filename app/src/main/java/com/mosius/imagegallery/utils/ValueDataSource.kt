package com.mosius.imagegallery.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

abstract class ValueDataSource<Value>(
    private val coroutinesScope: CoroutineScope = CoroutineScope(Dispatchers.Default)
) {
    private val lock = Any()
    private val data: LiveData<Value> = MutableLiveData()

    private var retry: (() -> Unit)? = null
    fun retry() = retry?.invoke()

    protected fun execute(getValue: suspend () -> Value) {
        if (!startLoadingSync()) return
        retry = null

        coroutinesScope.launch {
            try {
                val res = getValue.invoke()
                (data as MutableLiveData).postValue(res)
                stopLoading()
            } catch (exp: Exception) {
                exp.printStackTrace()
                retry = { execute(getValue) }
                this@ValueDataSource.error(exp)
            }
        }
    }

    fun toLiveData() = data

    val dataState: LiveData<LoadingState> = MutableLiveData()
    private var state: LoadingState = LoadingState.LOADED
    protected fun startLoadingSync(): Boolean = synchronized(lock) {
        if (state == LoadingState.LOADING) {
            false
        } else {

            startLoading()
            true
        }
    }

    private fun startLoading() {
        state = LoadingState.LOADING
        (dataState as MutableLiveData).postValue(state)
    }

    private fun stopLoading() {
        state = LoadingState.LOADED
        (dataState as MutableLiveData).postValue(state)
    }

    private fun error(exp: Exception?) {
        state = LoadingState.error(exp)
        (dataState as MutableLiveData).postValue(state)
    }
}