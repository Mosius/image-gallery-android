package com.mosius.imagegallery.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

abstract class PageKeyedDataSource<Key, Value>(
    private val loadInitial: (suspend (params: LoadInitialParams<Key>) -> LoadInitialResult<Key, Value>),
    private val loadBefore: (suspend (params: LoadParams<Key>) -> LoadResult<Key, Value>)? = null,
    private val loadAfter: (suspend (params: LoadParams<Key>) -> LoadResult<Key, Value>)? = null,
    private val coroutinesScope: CoroutineScope = CoroutineScope(Dispatchers.Default)
) : PageKeyedDataSource<Key, Value>() {

    private val lock = Any()
    private val mainScope = CoroutineScope(Dispatchers.Main)

    val loadingState: LiveData<LoadingState> = MutableLiveData()
    private var loading: LoadingState = LoadingState.LOADED
    private fun startLoadingSync(): Boolean = synchronized(lock) {
        if (loading == LoadingState.LOADING) {
            false
        } else {

            startLoading()
            true
        }
    }

    private fun startLoading() {
        loading = LoadingState.LOADING
        (loadingState as MutableLiveData).postValue(loading)
    }

    private fun stopLoading() {
        loading = LoadingState.LOADED
        (loadingState as MutableLiveData).postValue(loading)
    }

    private fun error(exp: Exception?) {
        loading = LoadingState.error(exp)
        (loadingState as MutableLiveData).postValue(loading)
    }

    final override fun loadInitial(
        params: LoadInitialParams<Key>,
        callback: LoadInitialCallback<Key, Value>
    ) {
        if (!startLoadingSync()) return
        retry = null
        coroutinesScope.launch {
            try {
                val res = loadInitial(params)
                stopLoading()
                mainScope.launch { callback.onResult(res.data, res.previousPageKey, res.nextPageKey) }
            } catch (exp: Exception) {
                retry = { loadInitial(params, callback) }
                this@PageKeyedDataSource.error(exp)
            }
        }
    }

    private var retry: (() -> Unit)? = null
    fun retry() = retry?.invoke()


    final override fun loadAfter(params: LoadParams<Key>, callback: LoadCallback<Key, Value>) {
        if (!startLoadingSync()) return
        retry = null
        coroutinesScope.launch {
            try {
                val res = loadAfter?.invoke(params)
                stopLoading()
                res?.let { mainScope.launch { callback.onResult(res.data, res.adjacentPageKey) } }
            } catch (exp: Exception) {
                retry = { loadAfter(params, callback) }
                this@PageKeyedDataSource.error(exp)
            }
        }
    }

    final override fun loadBefore(params: LoadParams<Key>, callback: LoadCallback<Key, Value>) {
        if (!startLoadingSync()) return
        retry = null
        coroutinesScope.launch {
            try {
                val res = loadBefore?.invoke((params))
                stopLoading()
                res?.let { mainScope.launch { callback.onResult(res.data, res.adjacentPageKey) } }
            } catch (exp: Exception) {
                retry = { loadBefore(params, callback) }
                this@PageKeyedDataSource.error(exp)
            }
        }
    }

    data class LoadInitialResult<Key, Value>(
        val data: List<Value>,
        val previousPageKey: Key?,
        val nextPageKey: Key?
    )

    data class LoadResult<Key, Value>(
        val data: List<Value>,
        val adjacentPageKey: Key?
    )
}