package com.mosius.imagegallery.presenters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class BaseFragment<V : AndroidViewModel> : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var vm: V

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(getLayout(), container, false)

    abstract fun getLayout(): Int

    override fun onAttach(context: Context) {
        super.onAttach(context)

        vm = ViewModelProvider(getViewModelOwner(), viewModelFactory)[getViewModelClass()]
    }

    abstract fun getViewModelClass(): Class<V>

    open fun getViewModelOwner(): ViewModelStoreOwner = requireActivity()
}