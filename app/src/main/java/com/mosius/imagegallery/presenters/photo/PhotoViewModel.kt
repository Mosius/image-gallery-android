package com.mosius.imagegallery.presenters.photo

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import javax.inject.Inject

class PhotoViewModel @Inject constructor(
    app: Application
) : AndroidViewModel(app)