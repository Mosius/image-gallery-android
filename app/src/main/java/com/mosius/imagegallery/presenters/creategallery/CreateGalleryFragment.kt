package com.mosius.imagegallery.presenters.creategallery

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mosius.imagegallery.R
import com.mosius.imagegallery.extensions.afterTextChanged
import com.mosius.imagegallery.extensions.toast
import com.mosius.imagegallery.presenters.BaseFragment
import com.mosius.imagegallery.utils.Status.*
import kotlinx.android.synthetic.main.fragment_create_gallery.*

class CreateGalleryFragment : BaseFragment<CreateGalleryViewModel>() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        edit_title.afterTextChanged { vm.title = it }
        edit_description.afterTextChanged { vm.description = it }
        edit_tags.afterTextChanged { vm.tags = it }

        button_create.setOnClickListener { vm.create() }
        vm.creatingState.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                RUNNING -> {
                    button_create.isClickable = false
                    progress_loading.visibility = View.VISIBLE
                }
                SUCCESS -> {
                    progress_loading.visibility = View.GONE
                }
                FAILED -> {
                    button_create.isClickable = true
                    progress_loading.visibility = View.GONE
                    it.exp?.message?.let(::toast)
                }
            }
        })

        vm.isCreatingFinish.observe(viewLifecycleOwner, Observer {
            findNavController().popBackStack()
        })
    }

    override fun getLayout() = R.layout.fragment_create_gallery

    override fun getViewModelClass() = CreateGalleryViewModel::class.java
}