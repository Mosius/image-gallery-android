package com.mosius.imagegallery.presenters

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind() {
        if (adapterPosition != RecyclerView.NO_POSITION) bind(adapterPosition)

    }

    protected abstract fun bind(position: Int)
}