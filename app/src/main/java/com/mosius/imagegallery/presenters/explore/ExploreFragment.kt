package com.mosius.imagegallery.presenters.explore

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mosius.imagegallery.R
import com.mosius.imagegallery.extensions.toast
import com.mosius.imagegallery.presenters.BaseFragment
import com.mosius.imagegallery.utils.Status.*
import kotlinx.android.synthetic.main.fragment_explore.*

class ExploreFragment : BaseFragment<ExploreViewModel>() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = GalleriesAdapter()
        recycler_galleries.adapter = adapter
        vm.galleries.observe(viewLifecycleOwner, Observer(adapter::submitList))

        adapter.setOnItemClickListener {
            findNavController().navigate(ExploreFragmentDirections.actionExploreFragmentToGalleryFragment(it))
        }

        layout_refresh.setOnRefreshListener(vm::refreshGallerie)
        layout_refresh.setColorSchemeColors(ContextCompat.getColor(requireContext(), R.color.colorAccent))

        vm.galleriesLoadingState.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                RUNNING -> {
                    progress_loading.visibility = View.VISIBLE
                }
                SUCCESS -> {
                    layout_refresh.isRefreshing = false
                    progress_loading.visibility = View.GONE
                }
                FAILED -> {
                    layout_refresh.isRefreshing = false
                    progress_loading.visibility = View.GONE
                    it.exp?.message?.let(::toast)
                }
            }
        })
    }

    override fun getLayout() = R.layout.fragment_explore

    override fun getViewModelClass() = ExploreViewModel::class.java

}