package com.mosius.imagegallery.presenters.sendpost

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.Transformations
import androidx.paging.toLiveData
import com.mosius.imagegallery.datasources.PhotosDataSource
import com.mosius.imagegallery.datasources.SendGalleryPostDataSource
import com.mosius.imagegallery.models.Gallery
import com.mosius.imagegallery.models.Photo
import javax.inject.Inject

class SendPostViewModel @Inject constructor(
    private val photosDataSource: PhotosDataSource,
    private val sendGalleryPostDataSource: SendGalleryPostDataSource,
    app: Application
) : AndroidViewModel(app) {

    private lateinit var gallery: Gallery

    fun init(gallery: Gallery) {
        this.gallery = gallery
    }

    val photos = photosDataSource.toLiveData(10)

    val finishPosting = sendGalleryPostDataSource.toLiveData()

    val postingState = sendGalleryPostDataSource.dataState

    val photosLoadingState = Transformations.switchMap(photosDataSource.source) { it.loadingState }

    fun refreshPhotos() = photosDataSource.source.value?.invalidate() ?: Unit

    fun retryPhotos() = photosDataSource.source.value?.retry() ?: Unit

    fun sendPost(photo: Photo) {
        sendGalleryPostDataSource.send(gallery.id, photo.id)
    }
}