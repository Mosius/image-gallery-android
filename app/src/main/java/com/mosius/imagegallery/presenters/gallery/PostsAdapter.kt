package com.mosius.imagegallery.presenters.gallery

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mosius.imagegallery.R
import com.mosius.imagegallery.models.Post
import com.mosius.imagegallery.models.PostStatus
import com.mosius.imagegallery.presenters.BaseViewHolder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_post.view.*

class PostsAdapter : PagedListAdapter<Post, BaseViewHolder>(DIFF_CALLBACK) {

    private var onOnItemClickListener: ((post: Post) -> Unit)? = null

    fun setItemClickListener(listener: (post: Post) -> Unit) {
        onOnItemClickListener = listener
    }

    private var onPostLikedListener: ((post: Post, liked: Boolean) -> Unit)? = null

    fun setOnPostLikedListener(listener: (post: Post, liked: Boolean) -> Unit) {
        onPostLikedListener = listener
    }

    private var onPostPublishedListener: ((post: Post) -> Unit)? = null

    fun setOnPostPublishedListener(listener: (post: Post) -> Unit) {
        onPostPublishedListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = with(LayoutInflater.from(parent.context)) {
        PhotoViewHolder(inflate(R.layout.item_post, parent, false), Picasso.get())
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.bind()

    inner class PhotoViewHolder(itemView: View, private val picasso: Picasso) : BaseViewHolder(itemView) {
        init {
            with(itemView) {
                check_like.setOnClickListener {
                    val position = if (adapterPosition != RecyclerView.NO_POSITION) adapterPosition else return@setOnClickListener
                    val item = getItem(position) ?: return@setOnClickListener
                    val liked = itemView.check_like.isChecked
                    onPostLikedListener?.invoke(item, liked)
                }
                button_publish.setOnClickListener {
                    val position = if (adapterPosition != RecyclerView.NO_POSITION) adapterPosition else return@setOnClickListener
                    val item = getItem(position) ?: return@setOnClickListener
                    onPostPublishedListener?.invoke(item)
                }

                setOnClickListener {
                    val position = if (adapterPosition != RecyclerView.NO_POSITION) adapterPosition else return@setOnClickListener
                    val item = getItem(position) ?: return@setOnClickListener
                    onOnItemClickListener?.invoke(item)
                }
            }
        }

        override fun bind(position: Int) = with(itemView) {
            val item = getItem(position) ?: return@with

            picasso.load(item.photo.url).into(image_photo)
            text_owner_name.text = item.photo.owner.fullName
            text_like_count.text = item.likes.toString()
            check_like.isChecked = item.liked ?: false

            if (item.status == PostStatus.REVIEW) {
                group_like.visibility = View.INVISIBLE
                button_publish.visibility = View.VISIBLE
            } else {
                group_like.visibility = View.VISIBLE
                button_publish.visibility = View.INVISIBLE
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Post>() {
            override fun areItemsTheSame(oldItem: Post, newItem: Post) = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Post, newItem: Post) = oldItem == newItem
        }
    }
}