package com.mosius.imagegallery.presenters.photos

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mosius.imagegallery.R
import com.mosius.imagegallery.extensions.toast
import com.mosius.imagegallery.presenters.BaseFragment
import com.mosius.imagegallery.utils.Status.*
import kotlinx.android.synthetic.main.fragment_photos.*

class PhotosFragment : BaseFragment<PhotosViewModel>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = PhotosAdapter()
        recycler_photos.adapter = adapter
        vm.photos.observe(viewLifecycleOwner, Observer(adapter::submitList))

        adapter.setOnItemClickListener {
            findNavController().navigate(PhotosFragmentDirections.actionPhotosFragmentToPhotoFragment(it))
        }

        layout_refresh.setOnRefreshListener(vm::refresh)
        layout_refresh.setColorSchemeColors(ContextCompat.getColor(requireContext(), R.color.colorAccent))

        vm.loadingState.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                RUNNING -> {
                    progress_loading.visibility = View.VISIBLE
                }
                SUCCESS -> {
                    layout_refresh.isRefreshing = false
                    progress_loading.visibility = View.GONE
                }
                FAILED -> {
                    layout_refresh.isRefreshing = false
                    progress_loading.visibility = View.GONE
                    it.exp?.message?.let(::toast)
                }
            }
        })

        fab_new_photo.setOnClickListener {
            findNavController().navigate(PhotosFragmentDirections.actionPhotosFragmentToNewPhotoFragment())
        }
    }

    override fun getLayout() = R.layout.fragment_photos

    override fun getViewModelClass() = PhotosViewModel::class.java
}