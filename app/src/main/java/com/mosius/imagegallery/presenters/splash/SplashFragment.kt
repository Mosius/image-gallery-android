package com.mosius.imagegallery.presenters.splash

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mosius.imagegallery.R
import com.mosius.imagegallery.presenters.BaseFragment

class SplashFragment : BaseFragment<SplashViewModel>() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm.userLoginState.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToPhotosFragment())
            } else {
                findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToRegisterFragment())
            }
        })
    }

    override fun getLayout() = R.layout.fragment_splash

    override fun getViewModelClass() = SplashViewModel::class.java
}