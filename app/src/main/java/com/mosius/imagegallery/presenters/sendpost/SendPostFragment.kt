package com.mosius.imagegallery.presenters.sendpost

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mosius.imagegallery.R
import com.mosius.imagegallery.extensions.toast
import com.mosius.imagegallery.models.Photo
import com.mosius.imagegallery.presenters.BaseFragment
import com.mosius.imagegallery.presenters.photos.PhotosAdapter
import com.mosius.imagegallery.utils.Status
import kotlinx.android.synthetic.main.fragment_send_post.*

class SendPostFragment : BaseFragment<SendPostViewModel>() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args = SendPostFragmentArgs.fromBundle(arguments!!)
        vm.init(args.gallery)

        val adapter = PhotosAdapter()
        recycler_photos.adapter = adapter
        vm.photos.observe(viewLifecycleOwner, Observer(adapter::submitList))

        adapter.setOnItemClickListener { showConfirmDialog(it) }

        vm.photosLoadingState.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.RUNNING -> {
                    progress_loading.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    progress_loading.visibility = View.GONE
                }
                Status.FAILED -> {
                    progress_loading.visibility = View.GONE
                    it.exp?.message?.let(::toast)
                }
            }
        })

        vm.postingState.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.RUNNING -> {
                    adapter.setOnItemClickListener { }
                }
                Status.SUCCESS -> {
                    adapter.setOnItemClickListener { photo -> showConfirmDialog(photo) }
                }
                Status.FAILED -> {
                    adapter.setOnItemClickListener { photo -> showConfirmDialog(photo) }
                }
            }
        })

        vm.finishPosting.observe(viewLifecycleOwner, Observer { findNavController().popBackStack() })
    }

    private fun showConfirmDialog(photo: Photo) {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.send_post_dialog_confirm_title)
            .setNegativeButton(R.string.confirm_dialog_no, null)
            .setPositiveButton(R.string.confirm_dialog_yes) { _, _ -> vm.sendPost(photo) }
            .show()
    }

    override fun getLayout() = R.layout.fragment_send_post

    override fun getViewModelClass() = SendPostViewModel::class.java

}