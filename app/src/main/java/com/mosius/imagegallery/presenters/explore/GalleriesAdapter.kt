package com.mosius.imagegallery.presenters.explore

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mosius.imagegallery.R
import com.mosius.imagegallery.models.Gallery
import com.mosius.imagegallery.presenters.BaseViewHolder
import kotlinx.android.synthetic.main.item_gallery.view.*
import kotlinx.android.synthetic.main.item_self_gallery.view.text_description
import kotlinx.android.synthetic.main.item_self_gallery.view.text_title

class GalleriesAdapter : PagedListAdapter<Gallery, BaseViewHolder>(DIFF_CALLBACK) {

    private var onItemClickListener: ((Gallery) -> Unit)? = null

    fun setOnItemClickListener(listener: (Gallery) -> Unit) {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = with(LayoutInflater.from(parent.context)) {
        PhotoViewHolder(inflate(R.layout.item_gallery, parent, false))
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.bind()

    inner class PhotoViewHolder(itemView: View) : BaseViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                val position = if (adapterPosition != RecyclerView.NO_POSITION) adapterPosition else return@setOnClickListener
                val item = getItem(position) ?: return@setOnClickListener
                onItemClickListener?.invoke(item)
            }
        }

        override fun bind(position: Int) = with(itemView) {
            val item = getItem(position) ?: return@with

            text_title.text = item.title
            text_description.text = item.description
            text_owner_name.text = item.owner.fullName
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Gallery>() {
            override fun areItemsTheSame(oldItem: Gallery, newItem: Gallery) = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Gallery, newItem: Gallery) = oldItem == newItem
        }
    }
}