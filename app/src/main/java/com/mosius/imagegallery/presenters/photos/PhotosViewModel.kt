package com.mosius.imagegallery.presenters.photos

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.Transformations
import androidx.paging.Config
import androidx.paging.toLiveData
import com.mosius.imagegallery.datasources.PhotosDataSource
import javax.inject.Inject

class PhotosViewModel @Inject constructor(
    private val photosDataSource: PhotosDataSource,
    app: Application
): AndroidViewModel(app) {

    val photos = photosDataSource.toLiveData(10)

    val loadingState = Transformations.switchMap(photosDataSource.source) { it.loadingState }

    fun refresh() = photosDataSource.source.value?.invalidate() ?: Unit

    fun retry() = photosDataSource.source.value?.retry() ?: Unit
}