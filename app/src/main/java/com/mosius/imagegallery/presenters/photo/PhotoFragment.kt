package com.mosius.imagegallery.presenters.photo

import android.os.Bundle
import android.view.View
import com.mosius.imagegallery.R
import com.mosius.imagegallery.presenters.BaseFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_photo.*

class PhotoFragment : BaseFragment<PhotoViewModel>() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args = PhotoFragmentArgs.fromBundle(arguments!!)

        val photo = args.photo

        Picasso.get().load(photo.url).into(image_photo)
        text_description.text = photo.description
        text_owner_name.text = photo.owner.fullName
    }

    override fun getLayout() = R.layout.fragment_photo

    override fun getViewModelClass() = PhotoViewModel::class.java
}