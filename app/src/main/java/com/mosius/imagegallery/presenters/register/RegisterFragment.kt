package com.mosius.imagegallery.presenters.register

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mosius.imagegallery.R
import com.mosius.imagegallery.extensions.afterTextChanged
import com.mosius.imagegallery.extensions.toast
import com.mosius.imagegallery.models.Gender
import com.mosius.imagegallery.presenters.BaseFragment
import com.mosius.imagegallery.utils.Status.*
import kotlinx.android.synthetic.main.fragment_register.*

class RegisterFragment : BaseFragment<RegisterViewModel>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        edit_email.afterTextChanged { vm.email = it }
        edit_password.afterTextChanged { vm.password = it }
        edit_first_name.afterTextChanged { vm.firstName = it }
        edit_last_name.afterTextChanged { vm.lastName = it }
        rgroup_gender.setOnCheckedChangeListener { group, _ ->
            vm.gender = when (group.checkedRadioButtonId) {
                R.id.radio_male -> Gender.MALE
                else -> Gender.FEMALE
            }
        }
        button_register.setOnClickListener { vm.register() }
        button_go_login.setOnClickListener { findNavController().navigate(RegisterFragmentDirections.actionRegisterFragmentToLoginFragment()) }

        vm.registerFinish.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(RegisterFragmentDirections.actionRegisterFragmentToLoginFragment())
        })

        vm.registeringState.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                RUNNING -> {
                    button_register.isClickable = false
                    progress_loading.visibility = View.VISIBLE
                }
                SUCCESS -> {
                    progress_loading.visibility = View.GONE
                }
                FAILED -> {
                    button_register.isClickable = true
                    progress_loading.visibility = View.GONE
                    it.exp?.message?.let(::toast)
                }
            }
        })
    }

    override fun getLayout() = R.layout.fragment_register

    override fun getViewModelClass() = RegisterViewModel::class.java
}