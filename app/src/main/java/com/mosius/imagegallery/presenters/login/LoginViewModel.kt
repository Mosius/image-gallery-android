package com.mosius.imagegallery.presenters.login

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.mosius.imagegallery.datasources.UserLoginDataSource
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val userLoginDataSource: UserLoginDataSource,
    app: Application
): AndroidViewModel(app) {
    val loginState = userLoginDataSource.dataState
    val loginFinish = userLoginDataSource.toLiveData()

    var email: String? = null
    var password: String? = null

    fun login() = userLoginDataSource.login(email, password)
}