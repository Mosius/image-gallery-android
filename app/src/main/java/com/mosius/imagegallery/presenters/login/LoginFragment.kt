package com.mosius.imagegallery.presenters.login

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mosius.imagegallery.R
import com.mosius.imagegallery.extensions.afterTextChanged
import com.mosius.imagegallery.extensions.toast
import com.mosius.imagegallery.presenters.BaseFragment
import com.mosius.imagegallery.utils.Status.*
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseFragment<LoginViewModel>() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        edit_email.afterTextChanged { vm.email = it }
        edit_password.afterTextChanged { vm.password = it }
        button_login.setOnClickListener { vm.login() }

        vm.loginState.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                RUNNING -> {
                    button_login.isClickable = false
                    progress_loading.visibility = View.VISIBLE
                }
                SUCCESS -> {
                    progress_loading.visibility = View.GONE
                }
                FAILED -> {
                    button_login.isClickable = true
                    progress_loading.visibility = View.GONE
                    it.exp?.message?.let(::toast)
                }
            }
        })

        vm.loginFinish.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToPhotosFragment())
        })
    }

    override fun getLayout() = R.layout.fragment_login

    override fun getViewModelClass() = LoginViewModel::class.java
}