package com.mosius.imagegallery.presenters.splash

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.mosius.imagegallery.datasources.UserLoginStateDataSource
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    userLoginStateDataSource: UserLoginStateDataSource,
    app: Application
) : AndroidViewModel(app) {

    val userLoginState = userLoginStateDataSource.toLiveData()
}