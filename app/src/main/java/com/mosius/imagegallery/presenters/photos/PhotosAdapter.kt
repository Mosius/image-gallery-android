package com.mosius.imagegallery.presenters.photos

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mosius.imagegallery.R
import com.mosius.imagegallery.models.Photo
import com.mosius.imagegallery.presenters.BaseViewHolder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_photo.view.*

class PhotosAdapter : PagedListAdapter<Photo, BaseViewHolder>(DIFF_CALLBACK) {
    private var onItemClickListener: ((Photo) -> Unit)? = null

    fun setOnItemClickListener(listener: (Photo) -> Unit) {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = with(LayoutInflater.from(parent.context)) {
        PhotoViewHolder(inflate(R.layout.item_photo, parent, false), Picasso.get())
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.bind()

    inner class PhotoViewHolder(itemView: View, private val picasso: Picasso) : BaseViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                val position = if (adapterPosition != RecyclerView.NO_POSITION) adapterPosition else return@setOnClickListener
                val item = getItem(position) ?: return@setOnClickListener
                onItemClickListener?.invoke(item)
            }
        }

        override fun bind(position: Int) = with(itemView) {
            val item = getItem(position) ?: return@with
            picasso.load(item.url).into(image_main)
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Photo>() {
            override fun areItemsTheSame(oldItem: Photo, newItem: Photo) = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Photo, newItem: Photo) = oldItem == newItem
        }
    }
}