package com.mosius.imagegallery.presenters.gallery

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mosius.imagegallery.R
import com.mosius.imagegallery.extensions.toast
import com.mosius.imagegallery.presenters.BaseFragment
import com.mosius.imagegallery.utils.Status
import kotlinx.android.synthetic.main.fragment_gallery.*

class GalleryFragment : BaseFragment<GalleryViewModel>() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args = GalleryFragmentArgs.fromBundle(arguments!!)
        vm.init(args.gallery)

        val adapter = PostsAdapter()
        recycler_posts.adapter = adapter
        vm.posts.observe(viewLifecycleOwner, Observer(adapter::submitList))
        adapter.setOnPostLikedListener(vm::likePost)
        adapter.setOnPostPublishedListener(vm::publishPost)

        layout_refresh.setOnRefreshListener(vm::refresh)
        layout_refresh.setColorSchemeColors(ContextCompat.getColor(requireContext(), R.color.colorAccent))

        adapter.setItemClickListener {
            findNavController().navigate(GalleryFragmentDirections.actionGalleryFragmentToPhotoFragment(it.photo))
        }

        vm.loadingState.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.RUNNING -> {
                    progress_loading.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    layout_refresh.isRefreshing = false
                    progress_loading.visibility = View.GONE
                }
                Status.FAILED -> {
                    layout_refresh.isRefreshing = false
                    progress_loading.visibility = View.GONE
                    it.exp?.message?.let(::toast)
                }
            }
        })

        fab_send_photo.setOnClickListener {
            findNavController().navigate(
                GalleryFragmentDirections.actionGalleryFragmentToSendPostFragment(vm.gallery)
            )
        }
    }

    override fun getLayout() = R.layout.fragment_gallery

    override fun getViewModelClass() = GalleryViewModel::class.java

}