package com.mosius.imagegallery.presenters.gallery

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import androidx.paging.toLiveData
import com.mosius.imagegallery.data.api.galleries.GalleriesApi
import com.mosius.imagegallery.datasources.LikePostDataSource
import com.mosius.imagegallery.datasources.PublishPostDataSource
import com.mosius.imagegallery.models.Gallery
import com.mosius.imagegallery.models.Post
import com.mosius.imagegallery.utils.PageKeyedDataSource
import javax.inject.Inject

class GalleryViewModel @Inject constructor(
    private val galleriesApi: GalleriesApi,
    app: Application
) : AndroidViewModel(app) {

    lateinit var gallery: Gallery

    fun init(gallery: Gallery) {
        this.gallery = gallery
    }

    fun likePost(post: Post, liked: Boolean) {
        val dataSource = LikePostDataSource(galleriesApi)

        val galleryId = gallery.id
        val postId = post.id

        if (liked) {
            dataSource.like(galleryId, postId)
        } else {
            dataSource.dislike(galleryId, postId)
        }

        dataSource.toLiveData().observeForever { if (it) refresh() }
    }

    fun publishPost(post: Post) {
        val dataSource = PublishPostDataSource(galleriesApi)

        val galleryId = gallery.id
        val postId = post.id

        dataSource.publish(galleryId, postId)

        dataSource.toLiveData().observeForever { refresh() }
    }

    private val galleryPostsDataSource = object : DataSource.Factory<String, Post>() {
        val source = MutableLiveData<PageKeyedDataSource<String, Post>>()

        override fun create() = object : PageKeyedDataSource<String, Post>(
            loadInitial = {
                val response = galleriesApi.getGalleryPosts(gallery.id)
                val posts = response.posts
                val lastKey = posts.lastOrNull()?.id
                LoadInitialResult(posts, null, lastKey)
            },
            loadAfter = {
                val response = galleriesApi.getGalleryPosts(gallery.id, it.key)
                val posts = response.posts
                val lastKey = posts.lastOrNull()?.id
                LoadResult(posts, lastKey)
            }
        ) {}.also(source::postValue)

    }

    val posts = galleryPostsDataSource.toLiveData(10)

    val loadingState = Transformations.switchMap(galleryPostsDataSource.source) { it.loadingState }

    fun refresh() = galleryPostsDataSource.source.value?.invalidate() ?: Unit

    fun retry() = galleryPostsDataSource.source.value?.retry() ?: Unit
}