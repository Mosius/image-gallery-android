package com.mosius.imagegallery.presenters.newphoto

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mosius.imagegallery.datasources.UploadPhotoDataSource
import java.io.File
import javax.inject.Inject

class NewPhotoViewModel @Inject constructor(
    private val uploadPhotoDataSource: UploadPhotoDataSource,
    app: Application
) : AndroidViewModel(app) {

    val uploadingState = uploadPhotoDataSource.dataState
    val isUploadFinish = uploadPhotoDataSource.toLiveData()

    val photoFile: LiveData<File> = MutableLiveData()
    var description: String? = null

    fun setPhotoFile(file: File) {
        (photoFile as MutableLiveData).postValue(file)
    }

    fun upload() {
        uploadPhotoDataSource.upload(
            photoFile.value,
            description
        )
    }

}