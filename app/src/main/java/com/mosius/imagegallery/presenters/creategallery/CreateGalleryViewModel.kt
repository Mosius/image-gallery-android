package com.mosius.imagegallery.presenters.creategallery

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.mosius.imagegallery.datasources.CreateGalleryDataSource
import javax.inject.Inject

class CreateGalleryViewModel @Inject constructor(
    private val createGalleryDataSource: CreateGalleryDataSource,
    app: Application
) : AndroidViewModel(app) {
    val creatingState = createGalleryDataSource.dataState
    val isCreatingFinish = createGalleryDataSource.toLiveData()

    var title: String? = null
    var description: String? = null
    var tags: String? = null

    fun create() {
        val tagsList = tags?.split(" ")?.filter { it.isNotBlank() }
        createGalleryDataSource.create(title, description, tagsList)
    }
}