package com.mosius.imagegallery.presenters

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor(
    app: Application
) : AndroidViewModel(app)