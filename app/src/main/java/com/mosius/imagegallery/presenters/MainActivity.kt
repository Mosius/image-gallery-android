package com.mosius.imagegallery.presenters

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.mosius.imagegallery.R
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var vm: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        vm = ViewModelProvider(this, viewModelFactory)[MainViewModel::class.java]

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController

        bottom_navigation_view.setupWithNavController(navController)
        toolbar.setupWithNavController(navController)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            bottom_navigation_view.visibility = when (destination.id) {
                R.id.registerFragment,
                R.id.loginFragment,
                R.id.splashFragment -> View.GONE
                else -> View.VISIBLE
            }
            toolbar.visibility = when (destination.id) {
                R.id.splashFragment -> View.GONE
                else -> View.VISIBLE
            }
        }
    }
}
