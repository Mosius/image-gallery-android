package com.mosius.imagegallery.presenters.selfgalleries

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.Transformations
import androidx.paging.toLiveData
import com.mosius.imagegallery.datasources.SelfGalleriesDataSource
import javax.inject.Inject

class SelfGalleriesViewModel @Inject constructor(
    private val selfGalleriesDataSource: SelfGalleriesDataSource,
    app: Application
) : AndroidViewModel(app) {

    val galleries = selfGalleriesDataSource.toLiveData(10)

    val loadingState = Transformations.switchMap(selfGalleriesDataSource.source) { it.loadingState }

    fun refresh() = selfGalleriesDataSource.source.value?.invalidate() ?: Unit

    fun retry() = selfGalleriesDataSource.source.value?.retry() ?: Unit
}