package com.mosius.imagegallery.presenters.newphoto

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mosius.imagegallery.R
import com.mosius.imagegallery.extensions.afterTextChanged
import com.mosius.imagegallery.extensions.toast
import com.mosius.imagegallery.presenters.BaseFragment
import com.mosius.imagegallery.utils.Status.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_new_photo.*
import java.io.File


class NewPhotoFragment : BaseFragment<NewPhotoViewModel>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button_pick_photo.setOnClickListener {
            if (getStoragePermission()) openPhotoChooser()
        }
        vm.photoFile.observe(viewLifecycleOwner, Observer {
            Picasso.get().load(it).into(image_photo)
        })
        edit_description.afterTextChanged { vm.description = it }
        button_upload.setOnClickListener { vm.upload() }
        vm.uploadingState.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                RUNNING -> {
                    button_upload.isClickable = false
                    button_pick_photo.isClickable = false
                    progress_loading.visibility = View.VISIBLE
                }
                SUCCESS -> {
                    progress_loading.visibility = View.GONE
                }
                FAILED -> {
                    button_upload.isClickable = true
                    button_pick_photo.isClickable = false
                    progress_loading.visibility = View.GONE
                    it.exp?.message?.let(::toast)
                }
            }
        })

        vm.isUploadFinish.observe(viewLifecycleOwner, Observer {
            findNavController().popBackStack()
        })
    }

    private fun getStoragePermission(): Boolean {
        val permissionState = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
        return if (permissionState != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), RQ_STORAGE_PERMISSION)
            false
        } else {
            true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == RQ_STORAGE_PERMISSION) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                openPhotoChooser()
            } else {
                toast(R.string.new_photo_permission_toast_text)
            }
        }
    }

    private fun openPhotoChooser() {
        val getIntent = Intent(Intent.ACTION_GET_CONTENT)
        getIntent.type = "image/*"

        val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        pickIntent.type = "image/*"

        val chooserIntent = Intent.createChooser(getIntent, getString(R.string.new_photo_chooser_title))
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))

        startActivityForResult(chooserIntent, RQ_PICK_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RQ_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            data ?: return
            val uri = data.data ?: return
            val path = getRealPathFromUri(requireContext(), uri) ?: return
            val file = File(path)
            vm.setPhotoFile(file)
        }
    }

    private fun getRealPathFromUri(context: Context, contentUri: Uri): String? {
        var cursor: Cursor? = null
        return try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = context.contentResolver.query(contentUri, proj, null, null, null)
            val columnIndex = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(columnIndex)
        } finally {
            cursor?.close()
        }
    }

    override fun getLayout() = R.layout.fragment_new_photo

    override fun getViewModelClass() = NewPhotoViewModel::class.java

    companion object {
        private const val RQ_PICK_IMAGE = 548
        private const val RQ_STORAGE_PERMISSION = 458
    }
}