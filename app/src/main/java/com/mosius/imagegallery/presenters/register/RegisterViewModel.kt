package com.mosius.imagegallery.presenters.register

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.mosius.imagegallery.datasources.UserRegisterDataSource
import com.mosius.imagegallery.models.Gender
import javax.inject.Inject

class RegisterViewModel @Inject constructor(
    private val registerDataSource: UserRegisterDataSource,
    app: Application
) : AndroidViewModel(app) {
    val registeringState = registerDataSource.dataState
    val registerFinish = registerDataSource.toLiveData()

    var email: String? = null
    var password: String? = null
    var firstName: String? = null
    var lastName: String? = null
    var gender: Gender? = null

    fun register() = registerDataSource.register(email, password, firstName, lastName, gender)
}
