package com.mosius.imagegallery.presenters.explore

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.Transformations
import androidx.paging.toLiveData
import com.mosius.imagegallery.datasources.GalleriesDataSource
import javax.inject.Inject

class ExploreViewModel @Inject constructor(
    private val galleriesDataSource: GalleriesDataSource,
    app: Application
) : AndroidViewModel(app) {

    val galleries = galleriesDataSource.toLiveData(10)

    val galleriesLoadingState = Transformations.switchMap(galleriesDataSource.source) { it.loadingState }

    fun refreshGallerie() = galleriesDataSource.source.value?.invalidate() ?: Unit

    fun retryGalleries() = galleriesDataSource.source.value?.retry() ?: Unit
}