package com.mosius.imagegallery.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mosius.imagegallery.presenters.MainViewModel
import com.mosius.imagegallery.presenters.creategallery.CreateGalleryViewModel
import com.mosius.imagegallery.presenters.explore.ExploreViewModel
import com.mosius.imagegallery.presenters.gallery.GalleryViewModel
import com.mosius.imagegallery.presenters.login.LoginViewModel
import com.mosius.imagegallery.presenters.newphoto.NewPhotoViewModel
import com.mosius.imagegallery.presenters.photo.PhotoViewModel
import com.mosius.imagegallery.presenters.photos.PhotosViewModel
import com.mosius.imagegallery.presenters.register.RegisterViewModel
import com.mosius.imagegallery.presenters.selfgalleries.SelfGalleriesViewModel
import com.mosius.imagegallery.presenters.sendpost.SendPostViewModel
import com.mosius.imagegallery.presenters.splash.SplashViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Inject
import javax.inject.Provider
import kotlin.reflect.KClass

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun mainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel::class)
    internal abstract fun registerViewModel(viewModel: RegisterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun loginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun splashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PhotosViewModel::class)
    internal abstract fun photosViewModel(viewModel: PhotosViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PhotoViewModel::class)
    internal abstract fun photoViewModel(viewModel: PhotoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewPhotoViewModel::class)
    internal abstract fun newPhotoViewModel(viewModel: NewPhotoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SelfGalleriesViewModel::class)
    internal abstract fun selfGalleriesViewModel(viewModel: SelfGalleriesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CreateGalleryViewModel::class)
    internal abstract fun createGalleryViewModel(viewModel: CreateGalleryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GalleryViewModel::class)
    internal abstract fun galleryViewModel(viewModel: GalleryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SendPostViewModel::class)
    internal abstract fun sendPostViewModel(viewModel: SendPostViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ExploreViewModel::class)
    internal abstract fun exploreViewModel(viewModel: ExploreViewModel): ViewModel
}

@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

class ViewModelFactory @Inject constructor(
    private val viewModels: MutableMap<Class<out ViewModel>, Provider<ViewModel>>
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        viewModels[modelClass]?.get() as T
}

@Module
abstract class ViewModelFactoryModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}