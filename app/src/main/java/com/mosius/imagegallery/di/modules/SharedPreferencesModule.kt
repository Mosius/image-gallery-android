package com.mosius.imagegallery.di.modules

import android.app.Application
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.mosius.imagegallery.di.SharedPreferencesAuth
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class SharedPreferencesModule {

    @Provides
    @Singleton
    @Named(SharedPreferencesAuth)
    internal fun provideAuthSharedPreferences(app: Application): SharedPreferences = app.getSharedPreferences("auth", MODE_PRIVATE)
}