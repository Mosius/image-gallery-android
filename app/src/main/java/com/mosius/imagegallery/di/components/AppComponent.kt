package com.mosius.imagegallery.di.components

import android.app.Application
import com.mosius.imagegallery.App
import com.mosius.imagegallery.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        MoshiModule::class,
        OkHttpModule::class,
        RestClientModule::class,
        AndroidBindingModule::class,
        SharedPreferencesModule::class,
        ViewModelModule::class,
        ViewModelFactoryModule::class,
        AndroidSupportInjectionModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

}