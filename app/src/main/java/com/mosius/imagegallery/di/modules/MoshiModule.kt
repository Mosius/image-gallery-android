package com.mosius.imagegallery.di.modules

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MoshiModule {

    @Provides
    @Singleton
    internal fun provideMoshi(): Moshi = Moshi.Builder()
        .build()
}