package com.mosius.imagegallery.di.modules

import com.mosius.imagegallery.BuildConfig
import com.mosius.imagegallery.data.api.galleries.GalleriesApi
import com.mosius.imagegallery.data.api.photoes.PhotosApi
import com.mosius.imagegallery.data.api.users.UsersApi
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class RestClientModule {

    @Provides
    @Singleton
    internal fun provideUsersApi(retrofit: Retrofit): UsersApi = retrofit.create(UsersApi::class.java)

    @Provides
    @Singleton
    internal fun providePhotosApi(retrofit: Retrofit): PhotosApi = retrofit.create(PhotosApi::class.java)

    @Provides
    @Singleton
    internal fun provideGalleriesApi(retrofit: Retrofit): GalleriesApi = retrofit.create(GalleriesApi::class.java)

    @Provides
    @Singleton
    internal fun provideRetrofit(
        moshi: Moshi,
        okHttpClient: OkHttpClient
    ): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.API_BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()
}