package com.mosius.imagegallery.di.modules

import com.mosius.imagegallery.presenters.MainActivity
import com.mosius.imagegallery.presenters.creategallery.CreateGalleryFragment
import com.mosius.imagegallery.presenters.explore.ExploreFragment
import com.mosius.imagegallery.presenters.gallery.GalleryFragment
import com.mosius.imagegallery.presenters.login.LoginFragment
import com.mosius.imagegallery.presenters.newphoto.NewPhotoFragment
import com.mosius.imagegallery.presenters.photo.PhotoFragment
import com.mosius.imagegallery.presenters.photos.PhotosFragment
import com.mosius.imagegallery.presenters.register.RegisterFragment
import com.mosius.imagegallery.presenters.selfgalleries.SelfGalleriesFragment
import com.mosius.imagegallery.presenters.sendpost.SendPostFragment
import com.mosius.imagegallery.presenters.splash.SplashFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AndroidBindingModule {

    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun registerFragment(): RegisterFragment

    @ContributesAndroidInjector
    abstract fun loginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun splashFragment(): SplashFragment

    @ContributesAndroidInjector
    abstract fun photosFragment(): PhotosFragment

    @ContributesAndroidInjector
    abstract fun photoFragment(): PhotoFragment

    @ContributesAndroidInjector
    abstract fun newPhotoFragment(): NewPhotoFragment

    @ContributesAndroidInjector
    abstract fun selfGalleriesFragment(): SelfGalleriesFragment

    @ContributesAndroidInjector
    abstract fun createGalleryFragment(): CreateGalleryFragment

    @ContributesAndroidInjector
    abstract fun galleryFragment(): GalleryFragment

    @ContributesAndroidInjector
    abstract fun sendPostFragment(): SendPostFragment

    @ContributesAndroidInjector
    abstract fun exploreFragment(): ExploreFragment
}