package com.mosius.imagegallery.datasources

import com.mosius.imagegallery.data.api.photoes.PhotosApi
import com.mosius.imagegallery.datasources.errors.UploadPhotoValidationError
import com.mosius.imagegallery.datasources.errors.ValidationError
import com.mosius.imagegallery.utils.ValueDataSource
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import javax.inject.Inject


class UploadPhotoDataSource @Inject constructor(
    private val photosApi: PhotosApi
) : ValueDataSource<Boolean>() {
    fun upload(
        photoFile: File?,
        description: String?
    ) = execute {
        val safePhotoFile = validatePhotoFile(photoFile)
        val safeDescription = validateDescription(description)

        val requestFile: RequestBody = safePhotoFile.asRequestBody("multipart/form-data".toMediaTypeOrNull())
        val imageBody: MultipartBody.Part = MultipartBody.Part.createFormData("photo", safePhotoFile.name, requestFile)

        val descriptionBody = safeDescription?.toRequestBody("application/json".toMediaTypeOrNull())

        photosApi.uploadPhoto(imageBody, descriptionBody, null)
        true
    }

    private fun validatePhotoFile(file: File?): File {
        if (file == null) throw ValidationError(UploadPhotoValidationError.PHOTO_REQUIRED.toString())
        return file
    }

    private fun validateDescription(description: String?): String? {
        if (description?.isBlank() == true) return null

        return description
    }
}