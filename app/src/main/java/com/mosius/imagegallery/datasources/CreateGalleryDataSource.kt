package com.mosius.imagegallery.datasources

import com.mosius.imagegallery.data.api.galleries.GalleriesApi
import com.mosius.imagegallery.data.api.galleries.ReqCreateGallery
import com.mosius.imagegallery.data.api.users.UsersApi
import com.mosius.imagegallery.datasources.errors.CreateGalleryValidationError
import com.mosius.imagegallery.datasources.errors.ValidationError
import com.mosius.imagegallery.utils.ValueDataSource
import javax.inject.Inject

class CreateGalleryDataSource @Inject constructor(
    private val galleriesApi: GalleriesApi
) : ValueDataSource<Boolean>() {
    fun create(
        title: String?,
        description: String?,
        tags: List<String>?
    ) = execute {
        val titleSafe = validateTitle(title)
        val descriptionSafe = validateDescription(description)

        val request = ReqCreateGallery(titleSafe, descriptionSafe, tags)
        galleriesApi.createGallery(request)
        true
    }

    private fun validateTitle(title: String?): String {
        if (title == null || title.isBlank()) throw ValidationError(CreateGalleryValidationError.TITLE_REQUIRED.toString())
        return title
    }

    private fun validateDescription(description: String?): String? {
        if (description?.isBlank() == true) return null

        return description
    }
}