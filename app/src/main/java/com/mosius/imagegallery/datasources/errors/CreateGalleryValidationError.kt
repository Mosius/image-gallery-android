package com.mosius.imagegallery.datasources.errors

enum class CreateGalleryValidationError {
    TITLE_REQUIRED;
}