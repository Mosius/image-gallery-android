package com.mosius.imagegallery.datasources

import com.mosius.imagegallery.data.sp.AuthSp
import com.mosius.imagegallery.utils.ValueDataSource
import javax.inject.Inject

class UserLoginStateDataSource @Inject constructor(
    authSp: AuthSp
) : ValueDataSource<Boolean>() {
    init {
        execute {
            val token = authSp.userToken
            token != null
        }
    }
}