package com.mosius.imagegallery.datasources.errors

enum class RegisterValidationError {
    EMAIL_REQUIRED,
    PASSWORD_REQUIRED,
    FIRST_NAME_REQUIRED,
    LAST_NAME_REQUIRED,
    GENDER_REQUIRED,
    EMAIL_FORMAT_WRONG,
    PASSWORD_FORMAT_WRONG,
    FIRST_NAME_FORMAT_WRONG,
    LASTNAME_FORMAT_WRONG;
}