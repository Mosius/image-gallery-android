package com.mosius.imagegallery.datasources

import com.mosius.imagegallery.data.api.galleries.GalleriesApi
import com.mosius.imagegallery.data.api.galleries.ReqSendGalleryPost
import com.mosius.imagegallery.utils.ValueDataSource
import javax.inject.Inject


class SendGalleryPostDataSource @Inject constructor(
    private val galleriesApi: GalleriesApi
) : ValueDataSource<Boolean>() {
    fun send(
        galleryId: String,
        photoId: String
    ) = execute {
        val body = ReqSendGalleryPost(photoId)
        galleriesApi.sendGalleryPost(galleryId, body)
        true
    }
}