package com.mosius.imagegallery.datasources

import android.util.Patterns
import com.mosius.imagegallery.data.api.users.ReqLogin
import com.mosius.imagegallery.data.api.users.UsersApi
import com.mosius.imagegallery.data.sp.AuthSp
import com.mosius.imagegallery.datasources.errors.LoginValidationError
import com.mosius.imagegallery.datasources.errors.ValidationError
import com.mosius.imagegallery.utils.ValueDataSource
import javax.inject.Inject

class UserLoginDataSource @Inject constructor(
    private val usersApi: UsersApi,
    private val authSp: AuthSp
) : ValueDataSource<Boolean>() {
    fun login(
        email: String?,
        password: String?
    ) = execute {
        val emailSafe = validateEmail(email)
        val passwordSafe = validatePassword(password)

        val request = ReqLogin(emailSafe, passwordSafe)
        val response = usersApi.login(request)

        authSp.userToken = response.token

        true
    }

    private fun validateEmail(email: String?): String {
        if (email == null || email.isBlank()) throw ValidationError(LoginValidationError.EMAIL_REQUIRED.toString())
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) throw ValidationError(LoginValidationError.EMAIL_FORMAT_WRONG.toString())
        return email
    }

    private fun validatePassword(password: String?): String {
        if (password == null || password.isBlank()) throw ValidationError(LoginValidationError.PASSWORD_REQUIRED.toString())
        if (password.length < 8) throw ValidationError(LoginValidationError.PASSWORD_FORMAT_WRONG.toString())
        return password
    }
}