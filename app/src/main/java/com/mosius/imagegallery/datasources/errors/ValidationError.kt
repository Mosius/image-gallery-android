package com.mosius.imagegallery.datasources.errors

class ValidationError(message: String? = null) : RuntimeException(message)

