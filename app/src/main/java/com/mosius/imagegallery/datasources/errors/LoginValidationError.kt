package com.mosius.imagegallery.datasources.errors

enum class LoginValidationError {
    EMAIL_REQUIRED,
    PASSWORD_REQUIRED,
    EMAIL_FORMAT_WRONG,
    PASSWORD_FORMAT_WRONG,
}