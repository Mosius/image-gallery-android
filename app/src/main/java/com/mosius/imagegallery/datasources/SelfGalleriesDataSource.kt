package com.mosius.imagegallery.datasources

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.mosius.imagegallery.data.api.users.UsersApi
import com.mosius.imagegallery.models.Gallery
import com.mosius.imagegallery.utils.PageKeyedDataSource
import javax.inject.Inject

class SelfGalleriesDataSource @Inject constructor(
    private val usersApi: UsersApi
) : DataSource.Factory<String, Gallery>() {
    val source = MutableLiveData<PageKeyedDataSource<String, Gallery>>()

    override fun create() = object : PageKeyedDataSource<String, Gallery>(
        loadInitial = {
            val response = usersApi.getSelfGalleries()
            val galleries = response.galleries
            val lastKey = galleries.lastOrNull()?.id
            LoadInitialResult(galleries, null, lastKey)
        },
        loadAfter = {
            val response = usersApi.getSelfGalleries(it.key)
            val photos = response.galleries
            val lastKey = photos.lastOrNull()?.id
            LoadResult(photos, lastKey)
        }
    ) {}.also(source::postValue)

}