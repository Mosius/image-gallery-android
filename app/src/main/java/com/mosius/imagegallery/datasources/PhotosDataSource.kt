package com.mosius.imagegallery.datasources

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.mosius.imagegallery.data.api.photoes.PhotosApi
import com.mosius.imagegallery.models.Photo
import com.mosius.imagegallery.utils.PageKeyedDataSource
import javax.inject.Inject

class PhotosDataSource @Inject constructor(
    private val photosApi: PhotosApi
) : DataSource.Factory<String, Photo>() {
    val source = MutableLiveData<PageKeyedDataSource<String, Photo>>()

    override fun create() = object : PageKeyedDataSource<String, Photo>(
        loadInitial = {
            val response = photosApi.getPhotos()
            val photos = response.photos
            val lastKey = photos.lastOrNull()?.id
            LoadInitialResult(photos, null, lastKey)
        },
        loadAfter = {
            val response = photosApi.getPhotos(it.key)
            val photos = response.photos
            val lastKey = photos.lastOrNull()?.id
            LoadResult(photos, lastKey)
        }
    ) {}.also(source::postValue)

}