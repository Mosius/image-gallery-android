package com.mosius.imagegallery.datasources

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.mosius.imagegallery.data.api.galleries.GalleriesApi
import com.mosius.imagegallery.data.api.photoes.PhotosApi
import com.mosius.imagegallery.models.Gallery
import com.mosius.imagegallery.models.Photo
import com.mosius.imagegallery.utils.PageKeyedDataSource
import javax.inject.Inject

class GalleriesDataSource @Inject constructor(
    private val galleriesApi: GalleriesApi
) : DataSource.Factory<String, Gallery>() {
    val source = MutableLiveData<PageKeyedDataSource<String, Gallery>>()

    override fun create() = object : PageKeyedDataSource<String, Gallery>(
        loadInitial = {
            val response = galleriesApi.getGalleries()
            val galleries = response.galleries
            val lastKey = galleries.lastOrNull()?.id
            LoadInitialResult(galleries, null, lastKey)
        },
        loadAfter = {
            val response = galleriesApi.getGalleries(it.key)
            val galleries = response.galleries
            val lastKey = galleries.lastOrNull()?.id
            LoadResult(galleries, lastKey)
        }
    ) {}.also(source::postValue)

}