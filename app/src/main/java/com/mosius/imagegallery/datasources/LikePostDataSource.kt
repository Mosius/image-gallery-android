package com.mosius.imagegallery.datasources

import com.mosius.imagegallery.data.api.galleries.GalleriesApi
import com.mosius.imagegallery.utils.ValueDataSource
import javax.inject.Inject

class LikePostDataSource @Inject constructor(
    private val galleriesApi: GalleriesApi
) : ValueDataSource<Boolean>() {
    fun like(
        galleryId: String,
        postId: String
    ) = execute {
        val response = galleriesApi.likeGalleryPost(galleryId, postId)
        response.new
    }

    fun dislike(
        galleryId: String,
        postId: String
    ) = execute {
        val response = galleriesApi.dislikeGalleryPost(galleryId, postId)
        response.new
    }
}