package com.mosius.imagegallery.datasources

import android.util.Patterns
import com.mosius.imagegallery.data.api.users.ReqRegister
import com.mosius.imagegallery.data.api.users.UsersApi
import com.mosius.imagegallery.datasources.errors.RegisterValidationError
import com.mosius.imagegallery.datasources.errors.ValidationError
import com.mosius.imagegallery.models.Gender
import com.mosius.imagegallery.utils.ValueDataSource
import javax.inject.Inject

class UserRegisterDataSource @Inject constructor(
    private val usersApi: UsersApi
) : ValueDataSource<Boolean>() {
    fun register(
        email: String?,
        password: String?,
        firstName: String?,
        lastName: String?,
        gender: Gender?
    ) = execute {
        val emailSafe = validateEmail(email)
        val passwordSafe = validatePassword(password)
        val firstNameSafe = validateFirstName(firstName)
        val lastNameSafe = validateLastName(lastName)
        val genderSafe = validateGender(gender)

        val request = ReqRegister(emailSafe, passwordSafe, firstNameSafe, lastNameSafe, genderSafe)
        usersApi.register(request)
        true
    }

    private fun validateEmail(email: String?): String {
        if (email == null || email.isBlank()) throw ValidationError(RegisterValidationError.EMAIL_REQUIRED.toString())
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) throw ValidationError(RegisterValidationError.EMAIL_FORMAT_WRONG.toString())
        return email
    }

    private fun validatePassword(password: String?): String {
        if (password == null || password.isBlank()) throw ValidationError(RegisterValidationError.PASSWORD_REQUIRED.toString())
        if (password.length < 8) throw ValidationError(RegisterValidationError.PASSWORD_FORMAT_WRONG.toString())
        return password
    }

    private fun validateFirstName(name: String?): String {
        if (name == null || name.isBlank()) throw ValidationError(RegisterValidationError.FIRST_NAME_REQUIRED.toString())
        return name
    }

    private fun validateLastName(name: String?): String {
        if (name == null || name.isBlank()) throw ValidationError(RegisterValidationError.LAST_NAME_REQUIRED.toString())
        return name
    }

    private fun validateGender(gender: Gender?): Gender {
        if (gender == null) throw ValidationError(RegisterValidationError.GENDER_REQUIRED.toString())
        return gender
    }
}