package com.mosius.imagegallery.datasources

import com.mosius.imagegallery.data.api.galleries.GalleriesApi
import com.mosius.imagegallery.models.Post
import com.mosius.imagegallery.utils.ValueDataSource
import javax.inject.Inject

class PublishPostDataSource @Inject constructor(
    private val galleriesApi: GalleriesApi
) : ValueDataSource<Post>() {
    fun publish(
        galleryId: String,
        postId: String
    ) = execute {
        val response = galleriesApi.publishGalleryPost(galleryId, postId)
        response.post
    }
}