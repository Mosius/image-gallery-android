package com.mosius.imagegallery.datasources.errors

enum class UploadPhotoValidationError {
    PHOTO_REQUIRED;
}