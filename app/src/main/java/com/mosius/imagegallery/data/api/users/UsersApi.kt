package com.mosius.imagegallery.data.api.users

import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface UsersApi {
    @POST("users")
    suspend fun register(
        @Body body: ReqRegister
    ): ResUser

    @POST("users/login")
    suspend fun login(
        @Body body: ReqLogin
    ): ResAuth

    @GET("users/galleries")
    suspend fun getSelfGalleries(
        @Query("lastId") lastId: String? = null
    ): ResGalleries
}