package com.mosius.imagegallery.data.api.galleries

data class ReqSendGalleryPost(
    val photo: String
)