package com.mosius.imagegallery.data.api.users

import com.mosius.imagegallery.models.User

data class ResUser(
    val user: User
)