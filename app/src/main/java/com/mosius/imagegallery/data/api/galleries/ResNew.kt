package com.mosius.imagegallery.data.api.galleries

data class ResNew(
    val new: Boolean
)