package com.mosius.imagegallery.data.api.galleries

import com.mosius.imagegallery.models.Post

data class ResPost(
    val post: Post
)