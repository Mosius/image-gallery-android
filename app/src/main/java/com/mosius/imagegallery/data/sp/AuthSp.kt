package com.mosius.imagegallery.data.sp

import android.content.SharedPreferences
import com.mosius.imagegallery.di.SharedPreferencesAuth
import javax.inject.Inject
import javax.inject.Named

class AuthSp @Inject constructor(
    @Named(SharedPreferencesAuth)
    private val sp: SharedPreferences
) {

    var userToken: String?
        get() = sp.getString("KEY_USER_TOKEN", null)
        set(value) = sp.edit().putString("KEY_USER_TOKEN", value).apply()

    fun clear() = sp.edit().clear().apply()
}