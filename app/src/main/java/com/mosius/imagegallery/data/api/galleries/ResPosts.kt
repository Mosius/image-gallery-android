package com.mosius.imagegallery.data.api.galleries

import com.mosius.imagegallery.models.Post

data class ResPosts(
    val posts: List<Post>
)