package com.mosius.imagegallery.data.api

import com.mosius.imagegallery.data.sp.AuthSp
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthorizationHeaderInterceptor @Inject constructor(
    private val authSp: AuthSp
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request().newBuilder()

        val token = authSp.userToken

        if (token != null) {
            originalRequest.addHeader(
                "Authorization",
                "Bearer $token"
            )
        }
        val newRequest = originalRequest.build()
        return chain.proceed(newRequest)
    }
}