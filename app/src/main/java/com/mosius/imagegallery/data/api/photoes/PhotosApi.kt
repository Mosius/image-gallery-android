package com.mosius.imagegallery.data.api.photoes

import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface PhotosApi {
    @GET("photos")
    suspend fun getPhotos(
        @Query("lastId") lastId: String? = null
    ): ResPhotos

    @POST("photos")
    @Multipart
    suspend fun uploadPhoto(
        @Part photo: MultipartBody.Part,
        @Part("description") description: RequestBody?,
        @Part("tags") tags: RequestBody?
    ): ResPhoto
}