package com.mosius.imagegallery.data.api.photoes

import com.mosius.imagegallery.models.Photo

data class ResPhotos(
    val photos: List<Photo>
)