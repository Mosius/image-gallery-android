package com.mosius.imagegallery.data.api.galleries

import com.mosius.imagegallery.models.Gallery

data class ResGalleries(
    val galleries: List<Gallery>
)