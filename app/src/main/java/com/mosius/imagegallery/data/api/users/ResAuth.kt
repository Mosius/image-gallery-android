package com.mosius.imagegallery.data.api.users

data class ResAuth(
    val token: String
)