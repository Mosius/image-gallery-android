package com.mosius.imagegallery.data.api.users

import com.mosius.imagegallery.models.Gallery

data class ResGalleries(
    val galleries: List<Gallery>
)