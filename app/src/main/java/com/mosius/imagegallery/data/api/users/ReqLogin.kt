package com.mosius.imagegallery.data.api.users

data class ReqLogin(
    val email: String,
    val password: String
)