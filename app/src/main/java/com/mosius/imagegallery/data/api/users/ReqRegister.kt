package com.mosius.imagegallery.data.api.users

import com.mosius.imagegallery.models.Gender

data class ReqRegister(
    val email: String,
    val password: String,
    val firstName: String,
    val lastName: String,
    val gender: Gender
)