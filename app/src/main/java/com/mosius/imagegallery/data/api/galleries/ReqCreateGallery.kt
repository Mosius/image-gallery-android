package com.mosius.imagegallery.data.api.galleries

data class ReqCreateGallery(
    val title: String,
    val description: String?,
    val tags: List<String>?
)