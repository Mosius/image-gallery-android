package com.mosius.imagegallery.data.api.galleries

import retrofit2.http.*

interface GalleriesApi {
    @GET("galleries")
    suspend fun getGalleries(
        @Query("lastId") lastId: String? = null,
        @Query("status[]") status: List<@JvmSuppressWildcards Any>? = null
    ): ResGalleries

    @POST("galleries")
    suspend fun createGallery(
        @Body body: ReqCreateGallery
    ): ResGallery

    @GET("galleries/{id}/posts")
    suspend fun getGalleryPosts(
        @Path("id") galleryId: String,
        @Query("lastId") lastId: String? = null
    ): ResPosts

    @POST("galleries/{id}/posts")
    suspend fun sendGalleryPost(
        @Path("id") galleryId: String,
        @Body body: ReqSendGalleryPost
    ): ResPost

    @PUT("galleries/{id}/posts/{postId}/like")
    suspend fun likeGalleryPost(
        @Path("id") galleryId: String,
        @Path("postId") postId: String
    ): ResNew

    @DELETE("galleries/{id}/posts/{postId}/like")
    suspend fun dislikeGalleryPost(
        @Path("id") galleryId: String,
        @Path("postId") postId: String
    ): ResNew

    @PATCH("galleries/{id}/posts/{postId}/publish")
    suspend fun publishGalleryPost(
        @Path("id") galleryId: String,
        @Path("postId") postId: String
    ): ResPost
}