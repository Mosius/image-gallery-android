package com.mosius.imagegallery.data.api.photoes

import com.mosius.imagegallery.models.Photo

data class ResPhoto(
    val photo: Photo
)