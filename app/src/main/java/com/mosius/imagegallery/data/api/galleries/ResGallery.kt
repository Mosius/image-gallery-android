package com.mosius.imagegallery.data.api.galleries

import com.mosius.imagegallery.models.Gallery

data class ResGallery(
    val gallery: Gallery
)