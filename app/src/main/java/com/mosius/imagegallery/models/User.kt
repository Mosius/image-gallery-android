package com.mosius.imagegallery.models

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    val id: String,
    val firstName: String,
    val lastName: String,
    val gender: Gender
) : Parcelable {
    @IgnoredOnParcel
    val fullName: String get() = "$firstName $lastName"
}

enum class Gender {
    MALE,
    FEMALE;
}