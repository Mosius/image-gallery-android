package com.mosius.imagegallery.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Gallery(
    val id: String,
    val title: String,
    val owner: User,
    val description: String?,
    val tags: List<String>?,
    val status: GalleryStatus
) : Parcelable

enum class GalleryStatus {
    ACTIVE,
    FINISHED;
}