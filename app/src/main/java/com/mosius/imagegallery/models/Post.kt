package com.mosius.imagegallery.models

data class Post(
    val id: String,
    val photo: Photo,
    val likes: Int,
    val liked: Boolean?,
    val status: PostStatus
)

enum class PostStatus {
    REVIEW,
    PUBLISHED,
    UNPUBLISHED;
}