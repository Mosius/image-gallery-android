package com.mosius.imagegallery.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Photo(
    val id: String,
    val url: String,
    val owner: User,
    val description: String?,
    val tags: List<String>?
): Parcelable